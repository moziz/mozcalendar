/*
import QtQuick 1.1
import QtWebKit 1.1
*/


import QtQuick 2.0
import QtWebKit 3.0


Rectangle {

	id: screen
	objectName: "screen"
	width: 800
	height: 450
	color: "#000000"
	radius: 0
	scale: 1
	onStateChanged: {
		refresh();
	}


	function showSettingsScreen(show) {
		console.debug("showSettingsScreen: " + show);
		calendarScreen.visible = !show;
		settingsScreen.visible = show;
	}

	function refresh() {
		console.debug("qml refresh");

		if (!calendarScreen.visible && !settingsScreen.visible)
			calendarScreen.visible = true;

		if (!calendar.isOperational()) {
			calendarRefreshTimer.running = true;
		} else
			calendarRefreshTimer.running = false;

		monthName.text = calendar.getMonthName();

		calendarName.text = calendar.getCalendarName();

		weekNumber1.text = calendar.getViewWeekNumber(0);
		weekNumber2.text = calendar.getViewWeekNumber(1);
		weekNumber3.text = calendar.getViewWeekNumber(2);
		weekNumber4.text = calendar.getViewWeekNumber(3);
		weekNumber5.text = calendar.getViewWeekNumber(4);
		weekNumber6.text = calendar.getViewWeekNumber(5);

		dateText.text = calendar.getSelectedDayNumber() + "." + calendar.getMonthNumber() + "." + calendar.getYear();
	}

	function showPleaseWaitBox(shown) {
		console.debug("showPleaseWaitBox");
		pleaseWaitBox.visible = shown;
	}


	Timer {
		interval: 10000; running: true; repeat: true; triggeredOnStart: false
		onTriggered: refresh();
	}

	Timer {
		id: calendarRefreshTimer
		interval: 1500; running: true; repeat: true; triggeredOnStart: true;
		onTriggered: calendar.refresh();
	}


	Rectangle {
		id: calendarScreen
		x: 0
		y: 0
		color: "#000000"
		anchors.rightMargin: 0
		anchors.bottomMargin: 0
		anchors.leftMargin: 0
		anchors.topMargin: 0
		anchors.fill: parent

		onVisibleChanged: {
			if (visible) {
				refresh();
			}
		}

		Text {
			id: dateText
			x: 489
			y: -125
			width: 256
			height: 48
			color: "#ffffff"
			text: qsTr("date")
			anchors.bottom: dayview.top
			anchors.bottomMargin: 8
			anchors.left: monthview.right
			anchors.leftMargin: 8
			anchors.right: parent.right
			anchors.rightMargin: 0
			font.bold: true
			font.pointSize: 18
			font.family: "Century Schoolbook L"
			transformOrigin: Item.BottomRight
			anchors.top: parent.top
			anchors.topMargin: 0
			horizontalAlignment: Text.AlignHCenter
			verticalAlignment: Text.AlignVCenter
		}

		GridView {
			id: monthview
			x: 0
			y: 0
			width: 448
			height: 378
			interactive: false
			anchors.bottom: parent.bottom
			anchors.left: parent.left
			anchors.top: parent.top
			anchors.bottomMargin: 0
			anchors.leftMargin: 24
			boundsBehavior: Flickable.StopAtBounds
			snapMode: GridView.SnapToRow
			anchors.topMargin: 72
			cellHeight: 64
			cellWidth: 64

			model: dataModel

			delegate:
				Item {
				width: 56
				height: 56
				MouseArea {
					anchors.fill: parent

					onClicked: {
						calendar.selectDay(yearNumber, monthNumber, dayNumber);
					}

					Rectangle {
						id: monthdaytile
						anchors.fill: parent
						gradient: Gradient {
							GradientStop {
								position: 0
								color: "#3e3e3e"
							}

							GradientStop {
								position: 1
								color: "#090909"
							}
						}

						// day tiles that do not belong to the currently selected month are faded out
						opacity: {
							if (monthNumber == calendar.getMonthNumber())
								return 1.0;
							return 0.3;
						}

						Text {
							id: daynumber
							y: 44
							height: 27
							anchors.top: parent.top
							anchors.topMargin: 8
							color: "#ffffff"
							text: dayNumber
	   anchors.left: parent.left
	   anchors.leftMargin: 0
	   anchors.right: parent.right
	   anchors.rightMargin: 0
							font.family: "Century Schoolbook L"
							style: Text.Normal
							font.bold: false
							horizontalAlignment: Text.AlignHCenter
							font.pixelSize: 20
						}



						Rectangle {
							id: extendedeventends
							height: 8
							width: 24
							x: -4
							anchors.bottom: parent.bottom
							anchors.bottomMargin: 4
							color: "gray"
							visible: (extendedEventEnds || extendedEventContinues)
						}

						Rectangle {
							id: extendedeventcontinues
							height: 8
							width: 16
							anchors.left: extendedeventends.right
							anchors.bottom: parent.bottom
							anchors.bottomMargin: 4
							color: "gray"
							visible: extendedEventContinues
						}

						Rectangle {
							id: extendedeventbegins
							height: 8
							width: 24

							anchors.left: extendedeventcontinues.right
							anchors.bottom: parent.bottom
							anchors.bottomMargin: 4
							color: "gray"
							visible: (extendedEventBegins || extendedEventContinues)
						}
						Rectangle {
							id: rectangle1
							x: 0
							width: 16
							height: 16
	   color: "#5c5c5c"
	   radius: 8
							anchors.horizontalCenter: parent.horizontalCenter
							anchors.top: daynumber.bottom
							anchors.topMargin: 0

							visible: {
								if (eventCount == 0)
									return false;
								return true;
							}

							Text {
								id: numberofevents
								color: "#ffffff"
								text: eventCount
								anchors.fill: parent

								font.family: "Century Schoolbook L"
								verticalAlignment: Text.AlignVCenter
								horizontalAlignment: Text.AlignHCenter
								font.pixelSize: 12
							}
						}
					}
				}
			}
		}

		ListView {
			id: dayview
   anchors.left: monthview.right
   anchors.leftMargin: 8
			boundsBehavior: Flickable.StopAtBounds
			anchors.bottom: toolbar.top
			anchors.bottomMargin: 0
			anchors.right: parent.right
			anchors.rightMargin: 0
			anchors.top: parent.top
			anchors.topMargin: 72
			spacing: 4

			model: dayViewDataModel

			delegate: Item {
				id: dayViewEvent
				height: 64
				width: 312
				Rectangle {
					id: jfkjfe
					gradient: Gradient {
						GradientStop {
							position: 0
							color: "#383838"
						}

						GradientStop {
							position: 1
							color: "#000000"
						}
					}




					anchors.fill: parent

					Rectangle {
						id: timelimits
						height: 16
	  anchors.left: parent.left
	  anchors.leftMargin: 0
	  anchors.right: parent.right
	  anchors.rightMargin: 0
						gradient: Gradient {
							GradientStop {
								position: 0
								color: "#474747"
							}

							GradientStop {
								position: 1
								color: "#1a1a1a"
							}
						}

						Text {
							id: starttime
							color: "#808080"
							text: startTime
							anchors.left: parent.left
							anchors.leftMargin: 4
							verticalAlignment: Text.AlignVCenter
							anchors.top: parent.top
							anchors.topMargin: 0
							anchors.bottom: parent.bottom
							anchors.bottomMargin: 0
						}

						Text {
							id: endtime
							color: "#808080"
							text: endTime
							font.bold: false
							verticalAlignment: Text.AlignVCenter
							horizontalAlignment: Text.AlignRight
							anchors.bottom: parent.bottom
							anchors.bottomMargin: 0
							anchors.top: parent.top
							anchors.topMargin: 0
							anchors.right: parent.right
							anchors.rightMargin: 4
						}
					}
					Text {
						id: summaryText
						color: "white"
						anchors.top: timelimits.bottom
						anchors.topMargin: 2

						text: summary
	  wrapMode: Text.WordWrap
	  verticalAlignment: Text.AlignVCenter
	  anchors.bottom: parent.bottom
	  anchors.bottomMargin: 4
						anchors.right: parent.right
						anchors.rightMargin: 4
						anchors.left: parent.left
						anchors.leftMargin: 4
						font.pointSize: 12
						font.bold: true
						horizontalAlignment: Text.AlignHCenter
					}

					/*
					Text {
						id: descriptionText
						color: "#808080"
						text: description
						anchors.right: parent.right
						anchors.rightMargin: 4
						anchors.left: parent.left
						anchors.leftMargin: 4
						anchors.bottom: parent.bottom
						anchors.bottomMargin: 4
						horizontalAlignment: Text.AlignLeft

						anchors.top: summaryText.bottom
						anchors.topMargin: 4

					}
					*/
				}

			}


		}

		Rectangle {
			id: toolbar
			color: "#00000000"
   anchors.left: monthview.right
   anchors.leftMargin: 8
   anchors.top: monthview.bottom
   anchors.topMargin: -32
			anchors.right: parent.right
			anchors.rightMargin: 0
			anchors.bottom: parent.bottom
			anchors.bottomMargin: 0

			Row {
				id: toolbar_row
				x: 183
				y: 0
	width: 129
	height: 32
	anchors.leftMargin: 183
				spacing: 16
				anchors.fill: parent

				MouseArea {
					id: button_today
					x: 0
					y: 0
					width: 32
					height: 32
					hoverEnabled: false
					Image {
						id: button_today_icon
						x: 0
						y: 0
	  width: 32
	  height: 32
						source: "images/arrow_down.png"
						sourceSize.width: 32
						fillMode: Image.Stretch
						sourceSize.height: 32
					}

					onClicked: calendar.selectToday();
				}

				MouseArea {
					id: button_settings
					x: 0
					y: 0
					width: 32
					height: 32
					hoverEnabled: false
					Image {
						id: button_settings_icon
						x: 0
						y: 0
						source: "images/settings.png"
						sourceSize.width: 32
						fillMode: Image.Stretch
						sourceSize.height: 32
					}

					onClicked: {
						showSettingsScreen(true);
					}
				}
				MouseArea {
					id: button_exit
					x: 0
					y: 0
					width: 32
					height: 32
					hoverEnabled: false

					onClicked: {
						Qt.quit();
					}

					Image {
						id: button_exit_icon
						x: 0
						y: 0
						sourceSize.height: 32
						sourceSize.width: 32
						fillMode: Image.Stretch
						source: "images/exit.png"
					}
				}



			}
		}

		Rectangle {
			id: calendarDisplayControls
			y: 0
			height: 24
			color: "#00000000"
			anchors.right: monthview.left
			anchors.rightMargin: -448
			anchors.left: parent.left
			anchors.leftMargin: 24
			anchors.bottom: weekdayLabels.top
			anchors.bottomMargin: 0

			MouseArea {
				id: button_prevMonth
				y: 0
				width: 24
				height: 24
				anchors.left: parent.left
				anchors.leftMargin: 0

				Image {
					id: button_prevMonth_icon
					anchors.fill: parent
					source: "images/arrow_left.png"
				}

				onClicked: {
					calendar.changeMonth(-1);
				}
			}

			Text {
				id: monthName
				color: "#ffffff"
				text: qsTr("month name")
				verticalAlignment: Text.AlignVCenter
				horizontalAlignment: Text.AlignHCenter
				anchors.bottom: parent.bottom
				anchors.bottomMargin: 0
				anchors.top: parent.top
				anchors.topMargin: 0
				anchors.right: button_nextMonth.left
				anchors.rightMargin: 0
				anchors.left: button_prevMonth.right
				anchors.leftMargin: 0
				font.bold: true
				font.family: "Century Schoolbook L"
				font.pixelSize: 16
			}

			MouseArea {
				id: button_nextMonth
				x: 472
				y: 1
				width: 24
				height: 24
				anchors.right: parent.right
				anchors.rightMargin: 0
				Image {
					id: button_nextMonth_icon
					anchors.fill: parent
					source: "images/arrow_right.png"
				}

				onClicked: {
					calendar.changeMonth(1);
				}
			}
		}

		Rectangle {
			id: weekdayLabels
			y: 25
			height: 23
			anchors.bottom: monthview.top
			anchors.bottomMargin: 0
			anchors.right: monthview.left
			anchors.rightMargin: -448
			anchors.left: parent.left
			anchors.leftMargin: 24
			gradient: Gradient {
				GradientStop {
					position: 0
					color: "#424242"
				}

				GradientStop {
					position: 1
					color: "#070707"
				}
			}

			Row {
				id: row2
	anchors.fill: parent
				spacing: 33

				Text {
					id: mon
					x: 0
					y: 0
					color: "#ffffff"
					text: qsTr("Mon")
					font.bold: true
					font.family: "Century Schoolbook L"
					font.pixelSize: 16
				}

				Text {
					id: tue
					x: 0
					y: 0
					color: "#ffffff"
					text: qsTr("Tue")
					font.pixelSize: 16
					font.family: "Century Schoolbook L"
					font.bold: true
				}

				Text {
					id: wed
					x: 0
					y: 0
					color: "#ffffff"
					text: qsTr("Wed")
					font.pixelSize: 16
					font.family: "Century Schoolbook L"
					font.bold: true
				}

				Text {
					id: thu
					x: 0
					y: 0
					color: "#ffffff"
					text: qsTr("Thu")
					font.pixelSize: 16
					font.family: "Century Schoolbook L"
					font.bold: true
				}

				Text {
					id: fri
					x: 0
					y: 0
					color: "#ffffff"
					text: qsTr("Fri")
					font.pixelSize: 16
					font.family: "Century Schoolbook L"
					font.bold: true
				}

				Text {
					id: sat
					x: 0
					y: 0
					color: "#ffffff"
					text: qsTr("Sat")
					font.pixelSize: 16
					font.family: "Century Schoolbook L"
					font.bold: true
				}

				Text {
					id: sun
					x: 0
					y: 0
					color: "#ffffff"
					text: qsTr("Sun")
					font.pixelSize: 16
					font.family: "Century Schoolbook L"
					font.bold: true
				}
			}
		}

		Rectangle {
			id: weekNumbers
			x: 25
			y: 48
			height: 402
			anchors.right: monthview.left
			anchors.rightMargin: 0
			gradient: Gradient {
				GradientStop {
					position: 0
					color: "#333333"
				}

				GradientStop {
					position: 1
					color: "#111111"
				}
			}
			anchors.bottom: parent.bottom
			anchors.bottomMargin: 0
			anchors.top: parent.top
			anchors.topMargin: 72
			anchors.left: parent.left
			anchors.leftMargin: 0

			Column {
				id: column1
				anchors.fill: parent
				spacing: 40






				Text {
					id: weekNumber1
					y: 0
					color: "#ffffff"
					text: qsTr("42")
					verticalAlignment: Text.AlignTop
					horizontalAlignment: Text.AlignHCenter
					anchors.left: parent.left
					anchors.leftMargin: 0
					anchors.right: parent.right
					anchors.rightMargin: 0
					font.family: "Century Schoolbook L"
					font.bold: true
					font.pixelSize: 16
				}
				Text {
					id: weekNumber2
					y: 0
					color: "#ffffff"
					text: qsTr("42")
					anchors.left: parent.left
					anchors.leftMargin: 0
					anchors.right: parent.right
					anchors.rightMargin: 0
					horizontalAlignment: Text.AlignHCenter
					font.pixelSize: 16
					font.bold: true
					font.family: "Century Schoolbook L"
				}
				Text {
					id: weekNumber3
					y: 0
					color: "#ffffff"
					text: qsTr("42")
					anchors.left: parent.left
					anchors.leftMargin: 0
					anchors.right: parent.right
					anchors.rightMargin: 0
					horizontalAlignment: Text.AlignHCenter
					font.pixelSize: 16
					font.bold: true
					font.family: "Century Schoolbook L"
				}
				Text {
					id: weekNumber4
					y: 0
					color: "#ffffff"
					text: qsTr("42")
					anchors.left: parent.left
					anchors.leftMargin: 0
					anchors.right: parent.right
					anchors.rightMargin: 0
					horizontalAlignment: Text.AlignHCenter
					font.pixelSize: 16
					font.bold: true
					font.family: "Century Schoolbook L"
				}
				Text {
					id: weekNumber5
					y: 0
					color: "#ffffff"
					text: qsTr("42")
					anchors.right: parent.right
					anchors.rightMargin: 0
					anchors.left: parent.left
					anchors.leftMargin: 0
					horizontalAlignment: Text.AlignHCenter
					font.pixelSize: 16
					font.bold: true
					font.family: "Century Schoolbook L"
				}
				Text {
					id: weekNumber6
					y: 0
					color: "#ffffff"
					text: qsTr("42")
					anchors.left: parent.left
					anchors.leftMargin: 0
					anchors.right: parent.right
					anchors.rightMargin: 0
					horizontalAlignment: Text.AlignHCenter
					font.pixelSize: 16
					font.bold: true
					font.family: "Century Schoolbook L"
				}
			}
		}

		Rectangle {
			id: switchCalendarControls
			x: 24
			y: 1
			height: 24
			color: "#00000000"
			MouseArea {
				id: button_prevCalendar
				y: 0
				width: 24
				height: 24
				Image {
					id: button_prevCalendar_icon
					anchors.fill: parent
					source: "images/arrow_left.png"
				}
				anchors.leftMargin: 0
				anchors.left: parent.left

				onClicked: calendar.previousCalendar();
			}

			Text {
				id: calendarName
				color: "#ffffff"
				text: calendar.getCalendarName();
				font.pixelSize: 16
				anchors.top: parent.top
				anchors.topMargin: 0
				anchors.bottom: parent.bottom
				anchors.rightMargin: 0
				anchors.bottomMargin: 0
				font.bold: true
				font.family: "Century Schoolbook L"
				anchors.right: button_nextCalendar.left
				horizontalAlignment: Text.AlignHCenter
				anchors.leftMargin: 0
				verticalAlignment: Text.AlignVCenter
				anchors.left: button_prevCalendar.right
			}

			MouseArea {
				id: button_nextCalendar
				x: 472
				y: 1
				width: 24
				height: 24
				Image {
					id: button_nextCalendar_icon
					anchors.fill: parent
					source: "images/arrow_right.png"
				}
				anchors.rightMargin: 0
				anchors.right: parent.right

				onClicked: calendar.nextCalendar();
			}
			anchors.bottom: weekdayLabels.top
			anchors.rightMargin: -448
			anchors.bottomMargin: 24
			anchors.right: monthview.left
			anchors.leftMargin: 24
			anchors.left: parent.left
		}
	}

	Rectangle {
		id: pleaseWaitBox
		width: 256
		height: 128
		radius: 16
		gradient: Gradient {
			GradientStop {
				position: 0
				color: "#80646464"
			}

			GradientStop {
				position: 1
				color: "#0d0d0d"
			}
		}
		anchors.horizontalCenter: parent.horizontalCenter
		anchors.verticalCenter: parent.verticalCenter
		x: 0
		y: 0
		opacity: 0.5
		visible: false

		Text {
			id: pleaseWaitText
			x: 0
			y: 0
			color: "#ffffff"
			text: "Please wait..."
			font.bold: true
			verticalAlignment: Text.AlignVCenter
			anchors.horizontalCenter: parent.horizontalCenter
			anchors.verticalCenter: parent.verticalCenter
			horizontalAlignment: Text.AlignHCenter
			font.pointSize: 24
		}
	}

	Rectangle {
		id: settingsScreen
		color: "#000000"
		anchors.fill: parent
		visible: false;

		Text {
			id: instructions
			x: 620
			y: 16
			color: "#ffffff"
			text: qsTr("Please log in with your Google account and accept the authorization request.")
			font.bold: false
			anchors.bottom: closeButton.top
			anchors.bottomMargin: 16
			anchors.right: parent.right
			anchors.rightMargin: 16
			wrapMode: Text.WordWrap
			anchors.top: parent.top
			anchors.topMargin: 16
			anchors.left: loginWebPage.right
			anchors.leftMargin: 16
			horizontalAlignment: Text.AlignHCenter
			verticalAlignment: Text.AlignTop
			styleColor: "#000000"
			font.pixelSize: 18
		}

		MouseArea {
			id: closeButton
			x: 607
			y: 401
			width: 177
			height: 33
			anchors.bottom: parent.bottom
			anchors.bottomMargin: 16
			anchors.right: parent.right
			anchors.rightMargin: 16

			onClicked: {
				showSettingsScreen(false);
			}

			Rectangle {
				id: closeButton_vis
				gradient: Gradient {
					GradientStop {
						position: 0
						color: "#424141"
					}

					GradientStop {
						position: 1
						color: "#0d0d0d"
					}
				}
				anchors.fill: parent
			}

			Text {
				id: closeButton_text
				color: "#ffffff"
				text: qsTr("close")
				verticalAlignment: Text.AlignVCenter
				horizontalAlignment: Text.AlignHCenter
				anchors.fill: parent
				font.pixelSize: 12
			}
		}

		WebView {
			objectName: "loginWebPage"
			id: loginWebPage
			x: 16
			y: 32
			anchors.rightMargin: 208
			anchors.leftMargin: 16
			anchors.bottomMargin: 16
			anchors.topMargin: 16
			anchors.fill: parent
			//contentsScale: 1
			//preferredHeight: 0
			//preferredWidth: 0
			url: ""

			onTitleChanged: {
				//oauth2.titleChanged(title)
				console.debug("WebView title changed to: " + title);
				if (title.substring(0,13) == "Success code=") {
					calendar.setAuthorizationCode(title.substring(13, title.length));
					url = "";
					showSettingsScreen(false);
					calendar.refresh();
				}
			}


			onVisibleChanged: {
				if (visible)
					url = calendar.getAuthorizationURL();
				else
					url = ""
			}
		}

	}
}
