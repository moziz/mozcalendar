#include "calendardata.h"

CalendarData::CalendarData(CalendarDataCache* dataCache, DayViewDataModel* dayView, QObject *parent)
{
	calDataCache = dataCache;
	dayViewData = dayView;

	for (int i = 0; i < 42; i++) {
		CalendarDayTile* tile = new CalendarDayTile();
		dayTileList.append(tile);
	}
/*
	QHash<int, QByteArray> roles;
	roles[dayNumberRole] = "dayNumber";
	roles[monthNumberRole] = "monthNumber";
	roles[yearNumberRole] = "yearNumber";
	roles[eventCountRole] = "eventCount";
	roles[extendedEventEndsRole] = "extendedEventEnds";
	roles[extendedEventContinuesRole] = "extendedEventContinues";
	roles[extendedEventBeginsRole] = "extendedEventBegins";
	roles[date] = "date";
	setRoleNames(roles);
	*/
}

void CalendarData::update(QDate* date) {
	qDebug() << "caldatat update";

	// calculates the number of weekdays before the selected month's 1st day. These are the overflow day tiles.
	// Firstday represents the first monday in the calendar view
	QDate firstday = QDate(date->year(), date->month(), 1);
	firstday = firstday.addDays(1-firstday.dayOfWeek());

	// setup the day tiles
	for (int i = 0; i < dayTileList.length(); i++) {
		if (firstday == *date)
			dayViewTile = dayTileList[i];

		// set date
		dayTileList[i]->setDate(firstday);
		firstday = firstday.addDays(1);

		// set event information
		dayTileList[i]->setEvents(calDataCache->getEventsForDay(firstday));
	}

	calDataCache->updateEvents(&dayTileList);

	//emit changed(); // not yet
}

void  CalendarData::dataProviderChanged() {
	emit dataChanged(createIndex(0,0), createIndex(dayTileList.length(),0));
	dayViewData->setDayTile(dayViewTile);
	emit changed();
}

////////////////////////
// QAbstractListModel //
////////////////////////


QHash<int, QByteArray> CalendarData::roleNames() const {
	QHash<int, QByteArray> roles;
	roles[dayNumberRole] = "dayNumber";
	roles[monthNumberRole] = "monthNumber";
	roles[yearNumberRole] = "yearNumber";
	roles[eventCountRole] = "eventCount";
	roles[extendedEventEndsRole] = "extendedEventEnds";
	roles[extendedEventContinuesRole] = "extendedEventContinues";
	roles[extendedEventBeginsRole] = "extendedEventBegins";
	roles[date] = "date";

	return roles;
}

int CalendarData::rowCount ( const QModelIndex & parent ) const {
	return dayTileList.length();
}

QVariant CalendarData::data ( const QModelIndex & index, int role ) const {
	switch (role) {
		case dayNumberRole:
			return dayTileList[index.row()]->getDayNumber();

		case monthNumberRole:
			return dayTileList[index.row()]->getMonthNumber();

		case yearNumberRole:
			return dayTileList[index.row()]->getYearNumber();

		case eventCountRole:
			return dayTileList[index.row()]->getNumberOfEvents();

		case extendedEventEndsRole:
			return dayTileList[index.row()]->getExtendedEventEnds();

		case extendedEventContinuesRole:
			return dayTileList[index.row()]->getExtendedEventContinues();

		case extendedEventBeginsRole:
			return dayTileList[index.row()]->getExtendedEventBegins();

		case date:
			return dayTileList[index.row()]->getDate();
	}

	return QVariant();
}
