#include "dayviewdatamodel.h"

DayViewDataModel::DayViewDataModel()
{
	dayTile = NULL;
	events = NULL;

	/*
	QHash<int, QByteArray> roles;
	roles[startTime] = "startTime";
	roles[endTime] = "endTime";
	roles[summary] = "summary";
	roles[description] = "description";
	setRoleNames(roles);
	*/
}

void DayViewDataModel::setDayTile(CalendarDayTile* tile) {
	beginResetModel();

	dayTile = tile;

	if (!dayTile)
		return;

	events = dayTile->getEvents();
	if (!events)
		return;

	endResetModel();
}

QHash<int,QByteArray> DayViewDataModel::roleNames() const {
	QHash<int, QByteArray> roles;
	roles[startTime] = "startTime";
	roles[endTime] = "endTime";
	roles[summary] = "summary";
	roles[description] = "description";

	return roles;
}

int DayViewDataModel::rowCount ( const QModelIndex & parent ) const {
	if (events)
		return events->length();
	return 0;
}

QVariant DayViewDataModel::data ( const QModelIndex & index, int role ) const {
	switch (role) {
		case startTime:
			return events->at(index.row())->timeBegins.toString(Qt::SystemLocaleShortDate);

		case endTime:
			return events->at(index.row())->timeEnds.toString(Qt::SystemLocaleShortDate);

		case summary:
			return events->at(index.row())->summary;

		case description:
			return events->at(index.row())->description;
	}

	return QVariant();
}
