#ifndef CALENDAR_H
#define CALENDAR_H

#include <QObject>
//#include <QDeclarativeContext>
//#include <QDeclarativeView>
#include <QQuickView>
#include <QQmlContext>
#include <QQuickItem>

#include <QDate>
#include <QGraphicsObject>
#include <QAbstractItemModel>

#include <QTimer>
#include <QThread>

#include "oauth2.h"
#include "calendardata.h"
#include "calendardatacache.h"
#include "dayviewdatamodel.h"

/**
 * @brief The Calendar class is the main controller which provides a clean interface to all the functions provided by the system.
 *
 * All user events go through this class. It also constructs the underlying object tree necessary to provide all the functions it claims to provide. It controls main.qml when necessary.
 */
class Calendar : public QObject
{
	Q_OBJECT

	/**
	 * @brief The OAuth2 object that should be used with this instance of Calendar
	 */
	OAuth2* oauth2;
	/**
	 * @brief The declarative context that hosts main.qml
	 */
	QQuickView* mainView;
	/**
	 * @brief The data model that is to be exposed to main.qml and shown in the day tile grid view
	 */
	CalendarData* calData;
	/**
	 * @brief The data provider that updates the view data models.
	 */
	CalendarDataCache* calDataCache;
	/**
	 * @brief The data model that is exposed to main.qml and displays the events of the selected date.
	 */
	DayViewDataModel* dayViewData;

	/**
	 * @brief The currently selected date.
	 *
	 * This is what determines what is shown. It's sort of an central variable that drives the whole system.
	 */
	QDate selectedDate;

public:
	/**
	 * @brief Constructs the calendar system and provides a clean interface to QML
	 *
	 * @param declView The Qt Quick view to be used
	 * @param parent The parent QObject
	 *
	 *  It also constructs the underlying object tree, connects signals and slots and introduces all relevant objects to the QML context
	 */
	explicit Calendar(QQuickView* declView, QObject *parent = 0);

	/**
	 * @brief Changes the selected month
	 *
	 * @param by Determines how many months should be added to the current month
	 *
	 * Causes the selected day to be changed to the first day of the current month + by.
	 */
	Q_INVOKABLE void changeMonth(int by);

	/**
	 * @brief Changes the selected day to the current day and refreshes the views.
	 *
	 *Calling this causes a refresh.
	 *
	 * @see selectDay()
	 */
	Q_INVOKABLE void selectToday();
	/**
	 * @brief Changes the selected day to the current day.
	 *
	 * @param year Year of the day to select.
	 * @param month Month of the day to select.
	 * @param day Day of the day to select.
	 *
	 * Calling this causes a refresh.
	 *
	 * @see selectDay()
	 */
	Q_INVOKABLE void selectDay(int year, int month, int day);
	/**
	 * @brief Causes events of a different Google Calendar to be shown.
	 *
	 * Calling this causes a refresh.
	 *
	 * @see previousCalendar()
	 */
	Q_INVOKABLE void nextCalendar();
	/**
	 * @brief Causes events of a different Google Calendar to be shown.
	 *
	 * Calling this causes a refresh.
	 *
	 * @see nextCalendar()
	 */
	Q_INVOKABLE void previousCalendar();
	/**
	 * @brief Refreshes the underlying data models to reflect the currently requested information
	 */
	Q_INVOKABLE void refresh();
	/**
	 * @brief A simple sanity check to find out whether or not the underlying structure should be capable of handling requests.
	 *
	 * @return true Everything is working as expected.
	 * @return false Something is wrong.
	 */
	Q_INVOKABLE bool isOperational();


	/**
	 * @brief Gets the URL to be shown in the authorization web browser
	 *
	 * @return URL for the authorization web page.
	 */
	Q_INVOKABLE QString getAuthorizationURL();
	/**
	 * @brief Sets the OAuth2 auhorization token to auhtCode.
	 *
	 * @param authCode New authorization token for OAuth2
	 */
	Q_INVOKABLE void setAuthorizationCode(QString authCode);

	/**
	 * @brief Returns the name of the currently selected month.
	 *
	 * @return Currently selected month's name.
	 */
	Q_INVOKABLE QString getMonthName();
	/**
	 * @brief Returns the number of the currently selected month.
	 *
	 * @return Currently selected month's number.
	 */
	Q_INVOKABLE int getMonthNumber();
	/**
	 * @brief Returns the number of the week the first day of the selected month belongs to.
	 *
	 * @return Week number for the first day of the selected month.
	 */
	Q_INVOKABLE int getFirstWeekNumber();
	/**
	 * @brief Returns the week number for the week that is calculated by adding addToCurrent to the first week of the selected month.
	 *
	 * Can be used to correctly produce a list of week numbers that follow the selected date.
	 *
	 * @return First week of this month + addToCurrent weeks
	 */
	Q_INVOKABLE int getViewWeekNumber(int addToCurrent);
	/**
	 * @brief Returns the day number of the selected date.
	 *
	 * @return The number of the selected day.
	 */
	Q_INVOKABLE int getSelectedDayNumber();
	/**
	 * @brief Returns the currently selected day.
	 *
	 * @return Currently selected day.
	 */
	QDate getSelectedDate();
	/**
	 * @brief Returns the year of the currently selected date.
	 *
	 * @return The year of the selected date.
	 */
	Q_INVOKABLE int getYear();
	/**
	 * @brief Returns the identification of the currently selected collection of events.
	 *
	 * @return The ID of the selected event collection.
	 */
	Q_INVOKABLE QString getCalendarName();

public slots:
	/**
	 * @brief When called causes the authorization request page to appear.
	 *
	 * This is itself non-destructive (authorization status is not lost).
	 */
	void requestAuthorization();
	/**
	 * @brief Informs Calendar that the underlying data models have been changed and notifies main.qml
	 */
	void dataChanged();
};

#endif // CALENDAR_H
