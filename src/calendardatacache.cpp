#include "calendardatacache.h"

CalendarDataCache::CalendarDataCache(OAuth2* oauth2, QObject *parent) :
	QObject(parent)
{
	this->oauth2 = oauth2;

	getCalendars();

	selectedCalendar = 0;
}

void CalendarDataCache::switchCalendar(int delta) {
	selectedCalendar = (selectedCalendar + delta) % calendarIds.length();

	qDebug() << "selected calendar index: " << selectedCalendar << " calendarids.length: " << calendarIds.length();
}

QString CalendarDataCache::getCalendarName() {
	if (selectedCalendar < calendarIds.length())
		return calendarIds[selectedCalendar];
	return "invalid calendar selection";
}

QList<CalendarEvent*>* CalendarDataCache::getEventsForDay(QDate date) {
	//calEvents.append(new CalendarEvent());
	if (calEvents.isEmpty())
		return NULL;

	QList<CalendarEvent*>* events = new QList<CalendarEvent*>(); // list of events that overlap with date

	QDateTime dayBegins = QDateTime(date, QTime(0,0,0));
	QDateTime dayEnds = QDateTime(date, QTime(23,59,59));

	// go through events and see which ones overlap with the chosen date
	for (int i = 0; i < calEvents.length(); i++) {
		CalendarEvent* event = calEvents[i];

		if (event->timeBegins <= dayBegins && event->timeEnds > dayBegins) {
			events->append(event);

		} else if (event->timeBegins >= dayBegins && event->timeBegins < dayEnds) {
			events->append(event);
		}
	}

	//qDebug() << "getEventsForDay count:" << events.length();
	return events;
}

QDateTime CalendarDataCache::parseDateTime(QVariantMap time, bool end) {
	QDateTime dt;

	QVariant t = time["dateTime"];
	if (t.isValid()) {
		dt = QDateTime::fromString(t.toString(), Qt::ISODate);
		return dt;
	}

	t = time["date"];
	if (t.isValid()) {
		dt = QDateTime::fromString(t.toString(), "yyyy-MM-dd");
		if (end) {
			dt = dt.addDays(-1);
			dt.setTime(QTime(23,59,59));
		} else {
			dt.setTime(QTime(0,0,0));
		}
	}

	return dt;
}

void CalendarDataCache::setEvents(QList<QVariant> events) {
	qDebug() << "Setting events";

	for (int i = 0; i < calEvents.length(); i++) {
		delete calEvents[i];
		calEvents.clear();
	}

	for (int i = 0; i < events.length(); i++) {
		QVariantMap incevent = events[i].toMap();
		CalendarEvent* event = new CalendarEvent();
		event->eventId = incevent["id"].toString();

		event->summary = incevent["summary"].toString();

		event->timeBegins = parseDateTime(incevent["start"].toMap(), false);
		event->timeEnds = parseDateTime(incevent["end"].toMap(), true);

		event->description = incevent["description"].toString();

		calEvents.append(event);

		qDebug() << "created event: id: " << event->eventId << " summary: " << event->summary << " time: " << event->timeBegins << " - " << event->timeEnds;
	}

	for (int i = 0; i < daysList->length(); i++) {
		CalendarDayTile* day = daysList->value(i);
		day->setEvents(getEventsForDay(day->getDate()));
	}

	emit changed();
}

void CalendarDataCache::getCalendars() {
	qDebug() << "Sending calendar list query";
	oauth2->sendRequest(OAuth2::GET, QNetworkRequest(QUrl("https://www.googleapis.com/calendar/v3/users/me/calendarList")));
}

void CalendarDataCache::updateEvents(QList<CalendarDayTile*>* days) {
	if (calendarIds.empty() || selectedCalendar > calendarIds.length() - 1) {
		selectedCalendar = 0;
		getCalendars();
		return;
	}

	daysList = days;

	// qt5
	//QUrl url = QUrl("https://www.googleapis.com/calendar/v3/calendars/" + calendarIds[selectedCalendar] + "/events");
	//url.addQueryItem("timeMin", QDateTime(days->first()->getDate(), QTime(0,0,0)).toString(Qt::ISODate) + "Z");
	//url.addQueryItem("timeMax", QDateTime(days->last()->getDate(), QTime(23,59,59)).toString(Qt::ISODate) + "Z");
	QUrl url = QUrl("https://www.googleapis.com/calendar/v3/calendars/" + calendarIds[selectedCalendar] + "/events");
	QUrlQuery query = QUrlQuery();
	query.addQueryItem("timeMin", QDateTime(days->first()->getDate(), QTime(0,0,0)).toString(Qt::ISODate) + "Z");
	query.addQueryItem("timeMax", QDateTime(days->last()->getDate(), QTime(23,59,59)).toString(Qt::ISODate) + "Z");
	url.setQuery(query);

	qDebug() << "CalendarDataCache sending event listing request to google...";
	oauth2->sendRequest(OAuth2::GET, QNetworkRequest(url));
}

void CalendarDataCache::input(QVariantMap json) {
	if (!json["kind"].isValid()) // check if the map represents a valid API reply
		return;

	QString kind = json["kind"].toString();

	if (kind == "calendar#calendarList") {
		QList<QVariant> calendars = json["items"].toList();
		for (int i = 0; i < calendars.length(); i++) {
			QString calendarId = calendars[i].toMap()["id"].toString();
			if (calendarId.contains('#')) // special calendars with '#' in their names will be discarded
				continue;

			calendarIds.append(calendarId);
		}
	} else if (kind == "calendar#events") {
		setEvents(json["items"].toList());
	}
}

bool CalendarDataCache::hasContent() {
	return !calendarIds.empty();
}
