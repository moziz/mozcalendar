#ifndef CALENDARDATA_H
#define CALENDARDATA_H

#include <QObject>
#include <QDebug>
#include <QDate>
#include <QAbstractListModel>
#include <QVariant>

#include "calendardaytile.h"
#include "calendardatacache.h"
#include "dayviewdatamodel.h"

/**
 * @brief Acts as the data model for the day grid view.
 *
 * This also handles updating the day specific event list when needed.
 */
class CalendarData : public QAbstractListModel {
	Q_OBJECT

	/**
	 * @brief The roles offered by this data model object.
	 */
	enum roles {
		dayNumberRole = Qt::UserRole + 1,
		monthNumberRole,
		yearNumberRole,
		eventCountRole,
		extendedEventEndsRole,
		extendedEventContinuesRole,
		extendedEventBeginsRole,
		date
	};

	/**
	 * @brief The CalendarDayTiles that represent days in the month view.
	 */
	QList<CalendarDayTile*> dayTileList;
	/**
	 * @brief Data provider.
	 */
	CalendarDataCache* calDataCache;
	/**
	 * @brief The data model that is used to show events of the selected date.
	 */
	DayViewDataModel* dayViewData;

	/**
	 * @brief The month day grid tile that represents the currently selected date.
	 */
	CalendarDayTile* dayViewTile;

public:
	/**
	 * @brief Constructs the list of CalendarDayTiles for the month day grid view.
	 *
	 * @param dataCache The data provider
	 * @param dayView The data model that is used to display events for the selected date
	 * @param parent The parent QObject
	 *
	 * The CalendarDayTiles are empty.
	 */
	explicit CalendarData(CalendarDataCache* dataCache, DayViewDataModel* dayView, QObject *parent = 0);

	/**
	 * @brief Updates the data model with new data retrieved from CalendarDataCache.
	 * @param date For which month should the data be.
	 *
	 * @todo Move the data updating functionality out of this method and integrate it in to CalendarDataCache to break a depency on Calendar. This would also make this class lighter as it should be.
	 */
	void update(QDate* date);

	QHash<int, QByteArray> roleNames() const;

	/**
	 * @brief Returns the number of data instances this data model currently offers.
	 * @param parent Unused.
	 * @return Number of day tiles available for showing.
	 */
	int rowCount ( const QModelIndex & parent ) const;
	/**
	 * @brief Used by QML to query
	 * @param index Index of the day tile instance in question
	 * @param role Role of a day tile
	 * @return Data of the specified day tile's role
	 */
	QVariant data ( const QModelIndex & index, int role ) const;

signals:
	/**
	 * @brief changed Emited when this data model has finished updating it self.
	 *
	 * It is used to inform the GUI about this event.
	 */
	void changed();

public slots:
	/**
	 * @brief dataProviderChanged Used by the data provider to inform CalendarData that it has updated the data referenced by this CalendarData object.
	 */
	void dataProviderChanged();

};

#endif // CALENDARDATA_H
