#ifndef CALENDARDAYTILE_H
#define CALENDARDAYTILE_H

#include <QObject>
#include <QDate>
#include <QDebug>
#include "calendarevent.h"

/**
 * @brief Represents a single day in the month view.
 *
 * Holds all the data required to draw the day and also keeps a list of all events that overlap this date.
 */
class CalendarDayTile : public QObject
{
	Q_OBJECT

	/**
	 * @brief The date this tile represents
	 */
	QDate date;
	/**
	 * @brief The number of events that overlap with this date
	 */
	int numberOfEvents;

	/**
	 * @brief True if an event partially overlaps with this date and ends on this day.
	 */
	bool extendedEventEnds;
	/**
	 * @brief True if an event partially overlaps with this date and begins on this day.
	 */
	bool extendedEventBegins;
	/**
	 * @brief True if an event overlaps with this date and neither begins nor ends on this day.
	 */
	bool extendedEventContinues;

	/**
	 * @brief List of all events overlapping with this date.
	 */
	QList<CalendarEvent*>* events;

public:
	/**
	 * @brief Constructs an day tile with the current date, extensions set to false, events set to null and number of events set to 0.
	 * @param parent Parent QObject
	 */
	explicit CalendarDayTile(QObject *parent = 0);

	/**
	 * @brief Returns the day number this day tile represents.
	 * @return Day number of this date.
	 */
	int getDayNumber();
	/**
	 * @brief Returns the month number this day tile represents.
	 * @return Month number of this date.
	 */
	int getMonthNumber();
	/**
	 * @brief Returns the year number this day tile represents.
	 * @return Year number of this date.
	 */
	int getYearNumber();

	/**
	 * @brief Returns the number of events that overlap with this date.
	 * @return Number of events during this day.
	 */
	int getNumberOfEvents();
	/**
	 * @brief Returns wether or not there is a event that begins before this day and ends on this day.
	 * @return true if there is a partially overlapping event that ends on this day.
	 * @return false otherwise.
	 */
	bool getExtendedEventEnds();
	/**
	 * @brief Returns wether or not there is a event that begins on this day and ends after this day.
	 * @return true if there is a partially overlapping event that begins on this day.
	 * @return false otherwise.
	 */
	bool getExtendedEventBegins();
	/**
	 * @brief Returns wether or not there is a event that begins before this day and ends after this day.
	 * @return true if there is a overlapping event that begins before and ends after this day.
	 * @return false otherwise.
	 */
	bool getExtendedEventContinues();
	/**
	 * @brief Returns the date this tile represents.
	 * @return The date of this tile
	 */
	QDate getDate();
	/**
	 * @brief Returns the list of events that overlap with this date.
	 * @return List of overlapping events.
	 */
	QList<CalendarEvent*>* getEvents();

	/**
	 * @brief Set the state of this tile according to the given even list.
	 * @param events List of events that overlap with this date. Should be NULL if no events overlap with this date.
	 *
	 * Takes in a list of events and updates the event count for this tile and sets the event extension flags.
	 */
	void setEvents(QList<CalendarEvent*>* events);
	/**
	 * @brief Sets the tile date to the given date.
	 * @param date A valid date.
	 */
	void setDate(QDate date);
};

#endif // CALENDARDAYTILE_H
