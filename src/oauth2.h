#ifndef OAUTH2_H
#define OAUTH2_H

#include <QObject>
#include <QDebug>

#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkReply>

#include <QDeclarativeProperty>
#include <QUrl>
#include <QDateTime>

//#include <qjson/parser.h>
#include <QJsonDocument>
#include <QVariant>
#include <QVariantMap>

#include <QFile>
#include <QDir>
//#include <QBuffer>

/**
 * @brief A simple OAuth2 and Google API client.
 *
 * Implements a simple OAuth2 client according to the documentation found here:
 * https://developers.google.com/accounts/docs/OAuth2InstalledApp
 *
 * Initial API access auhtorization. The user communicates with Google through a web browser window that is not handled by this class. The web browser has to supply the authorization code to this component by calling OAuth2::setAuthorizationCode() once it's received. After this has need done, this class can be used to communicate with
 *
 * API calls made through this class are automatically modified to include necessary credentials if available. It currently supports only GET and POST based queries. If API calls fail due to bad tokens, they are automatically refreshed if possible.
 *
 * This class should not be used with accounts or data that requirer confidentiality. Authorization tokens are saved on disk unecrypted. The save location is determined by OAuth2::serializationFileLocation.
 */

class OAuth2 : public QObject
{
	Q_OBJECT

	/**
	 * @brief netAccessMgr The QNetAccessManager object that should be used for all remote communication.
	 */
	QNetworkAccessManager* netAccessMgr;

	//QJson::Parser* parser;

	/**
	 * @brief URL for the authorization server.
	 */
	static const QString auth_endpoint;

	// Parameters used. Comments from Google's documentation
	/**
	 * @brief Used for client identification when communicating with Google.
	 */
	static const QString client_id;
	/**
	 * @brief How Google should deliver the access token.
	 *
	 * Currently using web browser title.
	 */
	static const QString redirect_uri;
	/**
	 * @brief Defines how much access the user should grant to their data held by Google.
	 *
	 * It is currently set to request read/write access to users calendars.
	 */
	static const QString scope;
	/**
	 * @brief This parameter is unused.
	 *
	 * If used, it would be reflected back by Google.
	 */
	static const QString state;
	/**
	 * @brief Used for client identification when communicating with Google.
	 */
	static const QString client_secret;

	/**
	 * @brief The current authorization code.
	 *
	 * This is what the web browser needs to supply this class to make it functional. It is refered to as "code" in the reply.
	 *
	 * It is received when a user consents to the access request. It is used to request access token request related information.
	 *
	 * @see setAuthorizationCode()
	 */
	QString authorization_code;
	/**
	 * @brief Current access token.
	 *
	 * It is used when making API calls. Each API call that requires user consent has to have this embedded in it. OAuth2 does this automatically when using sendRequest().
	 */
	QString access_token;
	/**
	 * @brief Used to request a new access token when the old one expires.
	 */
	QString refresh_token;
	/**
	 * @brief The time when the current access token expires.
	 *
	 * Currently unused.
	 */
	QDateTime expires;

	/**
	 * @brief Path to the folder where the serialization file should be located.
	 *
	 * Currently $home/.mozCalendar is used.
	 *
	 * @todo Allow changing.
	 */
	static const QString serializationFileLocation;

	/**
	 * @brief Sends an access token request to the auhtpoint.
	 */
	void requestAccessToken();
	/**
	 * @brief Sends request for a new access token using a refresh token.
	 */
	void requestAccessTokenRefresh();

	/**
	 * @brief Checks whether or not an access token is currently known.
	 * @return true Access token exists.
	 * @return false No access token exists.
	 */
	bool isValidAccessToken();


	/**
	 * @brief Attempts to deserialize it self from a text file in a set location.
	 *
	 * A file named "oauth2" should be located in the directory specified by OAuth2::serializationFileLocation. If the file is not present or it cannot be accessed, this method will return false.
	 *
	 * @return true Deserialization was successful.
	 * @return false Deserialization failed.
	 *
	 * @see serializeToFile()
	 * @see serializationFileLocation
	 *
	 * @todo Suppport encryption.
	 */
	bool deserializeFromFile();
	/**
	 * @brief Writes the current state to a text file.
	 *
	 * Fails silently if writing to file is not successful.
	 *
	 * @see deserializeFromFile()
	 * @see serializationFileLocation
	 *
	 * @todo Suppport encryption.
	 */
	void serializeToFile();

public:
	/**
	 * @brief Used to choose the HTTP verb to when making requests.
	 */
	enum httpMethod {
	//	DELETE,
	//	PUT,
	//	PATCH
		GET,
		POST
	};

	/**
	 * @brief Attempts to construct an OAuth2 object by deserializing it's state from disk.
	 *
	 * @param parent The parent QObject.
	 *
	 * If the deserialization fails, the constructed object will require calling setAuthorizationCode() before it is operational.
	 */
	explicit OAuth2(QObject *parent = 0);

	/**
	 * @brief Sends a HTTP request the the location specified.
	 *
	 * @param method The HTTP verb that should be used. Only accepts GET.
	 * @param request A valid Qt::QNetworkRequest object.
	 *
	 * Access token will be added to the request header before sending.
	 */
	void sendRequest(httpMethod method, QNetworkRequest request);
	/**
	 * @brief Sends a HTTP request that includes a body to the location specified.
	 *
	 * @param method The HTTP verb that should be used. Only accepts POST.
	 * @param request A valid Qt::QNetworkRequest object.
	 * @param body Data that should be included in the body of the HTTP request.
	 *
	 * @see sendRequest()
	 */
	void sendRequest(httpMethod method, QNetworkRequest request, QByteArray body);

	/**
	 * @brief Returns the URL for the web page on which a user consents to giving this application access to their data.
	 * @return URL to the Google API access granting page
	 */
	QString getAuthorizationURL();

	/**
	 * @brief Resets authorization by setting a new authorization code.
	 * @param newAuthCode The authorization token that should replace the current one.
	 *
	 * Access token and refresh token are discarded as they would not be valid after the authorization code anyway.
	 *
	 * Request for new refresh token and access token is sent.
	 */
	void setAuthorizationCode(QString newAuthCode);
	/**
	 * @brief Checks whether current tokens should be valid.
	 *
	 * Only checks whether or token are set to some value.
	 *
	 * @return true Tokens are present
	 * @return false Tokens are not present
	 */
	bool isAuthorized();

signals:
	/**
	 * @brief Emited when authorization code is missing and a user should grant this applications access to their Google data.
	 */
	void authorizatioRequired();
	/**
	 * @brief Emited when a JSON reply is received and it is not consumed by OAuth2 itself.
	 */
	void incomingData(QVariantMap);

public slots:
	/**
	 * @brief Receives replies from the network and passes them to relevant parties.
	 * @param reply A reply to HTTP request
	 */
	void handleNetworkReply(QNetworkReply* reply);
};

#endif // OAUTH2_H
