#include "calendardaytile.h"

CalendarDayTile::CalendarDayTile(QObject *parent) :
	QObject(parent)
{
	events = NULL;

	this->date = QDate::currentDate();
	numberOfEvents = 0;
	extendedEventBegins = false;
	extendedEventContinues = false;
	extendedEventEnds = false;
}

void CalendarDayTile::setDate(QDate date) {
	this->date = date;
}

void CalendarDayTile::setEvents(QList<CalendarEvent *>* eventList) {
	delete events;
	events = eventList;

	// update extended event markers
	extendedEventBegins = false;
	extendedEventContinues = false;
	extendedEventEnds = false;

	if (events == NULL)
		return;

	numberOfEvents = eventList->length();

	QDateTime dayBegins = QDateTime(date);
	QDateTime dayEnds = QDateTime(date, QTime(23,59,59));

	for (int i = 0; i < events->length(); i++) {
		CalendarEvent* event = events->at(i);

		// event: ####
		//   day:   ####
		if (!extendedEventEnds)
		if (event->timeBegins < dayBegins && event->timeEnds <= dayEnds)
			extendedEventEnds = true;

		// event: ########
		//   day:   ####
		if (!extendedEventContinues)
		if (event->timeBegins < dayBegins && event->timeEnds > dayEnds)
			extendedEventContinues = true;

		// event:     ####
		//   day:   ####
		if (!extendedEventBegins)
		if (event->timeBegins >= dayBegins && event->timeEnds > dayEnds)
			extendedEventBegins = true;
	}
}

int CalendarDayTile::getDayNumber() {
	return date.day();
}

int CalendarDayTile::getMonthNumber() {
	return date.month();
}

int CalendarDayTile::getYearNumber() {
	return date.year();
}

int CalendarDayTile::getNumberOfEvents() {
	return numberOfEvents;
}

bool CalendarDayTile::getExtendedEventEnds() {
	return extendedEventEnds;
}

bool CalendarDayTile::getExtendedEventContinues() {
	return extendedEventContinues;
}

bool CalendarDayTile::getExtendedEventBegins() {
	return extendedEventBegins;
}

QDate CalendarDayTile::getDate() {
	return date;
}

QList<CalendarEvent*>* CalendarDayTile::getEvents() {
	return events;
}
