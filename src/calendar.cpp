#include "calendar.h"

Calendar::Calendar(QQuickView* declView, QObject *parent) :
	QObject(parent)
{
	selectedDate = QDate::currentDate();
	mainView = declView;
	oauth2 = new OAuth2(this);
	calDataCache = new CalendarDataCache(oauth2);
	dayViewData = new DayViewDataModel();
	calData = new CalendarData(calDataCache, dayViewData);

	connect(oauth2, SIGNAL(authorizatioRequired()), this, SLOT(requestAuthorization()));
	connect(calData, SIGNAL(changed()), this, SLOT(dataChanged()));

	connect(oauth2, SIGNAL(incomingData(QVariantMap)), calDataCache, SLOT(input(QVariantMap)));
	connect(calDataCache, SIGNAL(changed()), calData, SLOT(dataProviderChanged()));

	mainView->rootContext()->setContextProperty("calendar", this);
	mainView->rootContext()->setContextProperty("dataModel", calData);
	mainView->rootContext()->setContextProperty("dayViewDataModel", dayViewData);

	selectToday();
}


QDate Calendar::getSelectedDate() {
	return selectedDate;
}

void Calendar::requestAuthorization() {
	QMetaObject::invokeMethod(mainView->rootObject(), "showSettingsScreen", Q_ARG(QVariant, true));

}

void Calendar::changeMonth(int by) {
	selectedDate.setDate(selectedDate.year(), selectedDate.month(), 1); // select the first day to avoid problems when going from 31 to 30 day month
	selectedDate = selectedDate.addMonths(by);
	qDebug() << "Current month:" << selectedDate.month() << "year:" << selectedDate.year();

	refresh();
}

void Calendar::nextCalendar() {
	calDataCache->switchCalendar(1);
}

void Calendar::previousCalendar() {
	calDataCache->switchCalendar(-1);
}

void Calendar::refresh() {
	qDebug() << "calendar refresh";

	QMetaObject::invokeMethod(mainView->rootObject(), "showPleaseWaitBox", Q_ARG(QVariant, true));
	calData->update(&selectedDate);
}

bool Calendar::isOperational() {
	bool operational = true;

	if (!oauth2->isAuthorized()) {
		requestAuthorization();
		operational = false;
	}

	if (!calDataCache->hasContent()) {
		calDataCache->getCalendars();
		operational = false;
	}

	return operational;
}

void Calendar::setAuthorizationCode(QString authCode) {
	oauth2->setAuthorizationCode(authCode);
}

QString Calendar::getAuthorizationURL() {
	return oauth2->getAuthorizationURL();
}

QString Calendar::getMonthName() {
	return QDate::longMonthName(selectedDate.month(), QDate::StandaloneFormat);
}

int Calendar::getMonthNumber() {
	return selectedDate.month();
}

int Calendar::getYear() {
	return selectedDate.year();
}

int Calendar::getSelectedDayNumber() {
	return selectedDate.day();
}

int Calendar::getFirstWeekNumber() {
	return QDate(selectedDate.year(), selectedDate.month(), 1).weekNumber();
}

int Calendar::getViewWeekNumber(int addToCurrent) {

	return QDate(selectedDate.year(), selectedDate.month(), 1).addDays(7*addToCurrent).weekNumber();
}

QString Calendar::getCalendarName() {
	return calDataCache->getCalendarName();
}

void Calendar::selectToday() {
	selectDay(QDate::currentDate().year(), QDate::currentDate().month(), QDate::currentDate().day());
}

void Calendar::selectDay(int year, int month, int day) {
	selectedDate = QDate(year, month, day);
	refresh();
}

//////////////////
// PUBLIC SLOTS //
//////////////////

void Calendar::dataChanged() {
	qDebug() << "calendar updating qml stuffs";
	QMetaObject::invokeMethod(mainView->rootObject(), "refresh");
	QMetaObject::invokeMethod(mainView->rootObject(), "showPleaseWaitBox", Q_ARG(QVariant, false));
}
