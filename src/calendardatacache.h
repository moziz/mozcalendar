#ifndef CALENDARDATACACHE_H
#define CALENDARDATACACHE_H

#include <QObject>
#include <QDebug>
#include <qjson/parser.h>
#include <QUrlQuery>

#include "calendarevent.h"
#include "oauth2.h"
#include "calendardaytile.h"

/**
 * @brief Communicates with Google to provide data for the display data models.
 *
 * Makes API calls through OAuth2 and parses replies in to internal data structures.
 */
class CalendarDataCache : public QObject
{
	Q_OBJECT

	/**
	 * @brief The OAuth2 instance that should be used for all remote communication
	 */
	OAuth2* oauth2;
	/**
	 * @brief A list of CalendarEvents that holds all currently known events.
	 */
	QList<CalendarEvent*> calEvents;
	/**
	 * @brief A list of IDs with which events can be queried.
	 */
	QList<QString> calendarIds;

	/**
	 * @brief A list of day tiles to be updated when new data is received.
	 */
	QList<CalendarDayTile*>* daysList;

	/**
	 * @brief Index on the calendarIds list of the currently selected calendar.
	 */
	int selectedCalendar;



public:
	/**
	 * @brief Constructs a CalendarDataCache with selected calendar set to 0 and sends a request for calendar names.
	 * @param oauth2 Provides access to Google API.
	 * @param parent The parent QObject.
	 */
	explicit CalendarDataCache(OAuth2* oauth2, QObject *parent = 0);

	/**
	 * @brief Returns a list of events that overlap with a given date.
	 * @param date The date for which the overlapping event should be returned
	 * @return NULL if no events overlap with the given date.
	 * @return A list of events that overlap with the given date.
	 */
	QList<CalendarEvent*>* getEventsForDay(QDate date);

	/**
	 * @brief Takes in a list of event resources, constructs CalendarEvents out of them and stores them.
	 * @param events A list of calendar event resources as specified in the .
	 * It expects the QVariants to be event maps as specified in the Google Calendar API documentation.
	 *
	 * It expects to find atleast "id", "summary", "start", "end" and "description" attributes and their values.
	 *
	 * The CalendarDayTile list given when calling updateEvents is updated with these newly constructed events.
	 *
	 * @see updateEvents()
	 *
	 * @todo Rather than modifying the CalendarDayTiles in this method, this method should merely inform all interested parties that it has new data available. This lets them ask for the changes when they need/want them and not be forced to accept the changes immediately.
	 */
	void setEvents(QList<QVariant> events);
	/**
	 * @brief Queries Google for events that overlap with the given days in the selected calendar.
	 * @param days Sorted (ascending order) list of dates.
	 *
	 * Takes the date of the first and the last entry of the list and queries Google for events that overlap with this timeframe.
	 *
	 * The list of CalendarDayTiles is saved for the time when a reply is received and
	 *
	 * @see setEvents()
	 *
	 * @todo At the moment this method does two thing that should not be in a single method. One would have to split this method in to two by separating the query sending in to its own method and make a new method that takes in the date list after a reply to this query has been received. This method should only take in the start and end time of the timeframe for which the events are desired with no knowledge where the events will eventually end up in.
	 */
	void updateEvents(QList<CalendarDayTile*>* days);

	/**
	 * @brief Changes the selected calendar ID used for querying events.
	 * @param delta By how much should the calendar selection be changed. Can be any negative or positive number.
	 *
	 * The selection will wrap around when going over or under the number of calendar ids known.
	 */
	void switchCalendar(int delta);
	/**
	 * @brief Returns the ID of the currently selected calendar.
	 * @return Currently selected calendars ID
	 */
	QString getCalendarName();

	/**
	 * @brief Sends a calendar list query to Google.
	 */
	void getCalendars();
	/**
	 * @brief Checks whether or not any calendar IDs are known.
	 * @return true if atleast one calendar ID is known.
	 * @return false if no calendar IDs are known.
	 */
	bool hasContent();

	/**
	 * @brief Parses a Google "date" or a "dateTime" field in to a QDateTime.
	 * @param time Either a Google "date" or a "dateTime" field.
	 * @param end Whether or not this "date" should be considered an endind date. Has no effect when "dateTime" resource is given as time.
	 *
	 * Google considers ending dates to be exclusive, so a ending date of 2013-01-31 will become 2013-01-30T23:59:59.
	 *
	 * @return A QDateTime time representation of the input.
	 */
	static QDateTime parseDateTime(QVariantMap time, bool end);
signals:
	/**
	 * @brief The data held by this object has been changed.
	 */
	void changed();

public slots:
	/**
	 * @brief Takes a Google API reply and parses all relevant information out of it.
	 * @param json A reply to one of the requests made by this object.
	 *
	 * If the reply cannot be handled by this, it is discarded.
	 */
	void input(QVariantMap json);
};

#endif // CALENDARDATACACHE_H
