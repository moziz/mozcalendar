#ifndef DAYVIEWDATAMODEL_H
#define DAYVIEWDATAMODEL_H

#include <QAbstractListModel>
#include <QDate>

#include "calendardatacache.h"
#include "calendardaytile.h"

/**
 * @brief Data model for the date specific event list.
 *
 * Extracts it's from a user selected CalendarDayTile and passes it's event data to QML.
 */
class DayViewDataModel : public QAbstractListModel
{
	Q_OBJECT

	/**
	 * @brief The roles of CalendarEvent that are accessible through this data model.
	 */
	enum roles {
		startTime = Qt::UserRole + 1,
		endTime,
		summary,
		description
	};

	/**
	 * @brief The day tile that holds the list of events to be made available through this model.
	 */
	CalendarDayTile* dayTile;
	/**
	 * @brief The list of events of events extracted from the CalendarDayTile.
	 */
	QList<CalendarEvent*>* events;

public:
	/**
	 * @brief Constructs a DayViewDataModel with all properties set to NULL.
	 */
	DayViewDataModel();

	/**
	 * @brief If possible extracts events from the given CalendarDayTile.
	 * @param tile A CalendarDayTile which' events should be made available through this model.
	 */
	void setDayTile(CalendarDayTile* tile);

	QHash<int,QByteArray> roleNames() const;

	/**
	 * @brief Returns the number of events available through this model.
	 * @param parent Unused. Can be anything.
	 * @return The number of events available.
	 */
	int rowCount ( const QModelIndex & parent ) const;
	/**
	 * @brief Returns the value of the given role for the event specified by the given index.
	 * @param index Index of the event instance. Only row() is used.
	 * @param role The role for which it's value is to be returned.
	 * @return Value of the given role for the given index.
	 */
	QVariant data ( const QModelIndex & index, int role ) const ;
};

#endif // DAYVIEWDATAMODEL_H
