#include <QtGui/QGuiApplication>
#include <QApplication>

#include "calendar.h"
//#include "qmlapplicationviewer.h"
#include "qtquick2applicationviewer.h"

//Q_DECL_EXPORT int main(int argc, char *argv[]) {
int main(int argc, char *argv[]) {
	//QScopedPointer<QApplication> app(createApplication(argc, argv));
	QApplication app(argc, argv);

	//QmlApplicationViewer viewer;
	QtQuick2ApplicationViewer viewer;
	//viewer.setOrientation(QmlApplicationViewer::ScreenOrientationAuto);

	Calendar cal(&viewer);

	//viewer.setMainQmlFile(QLatin1String("qml/kalenteri/main.qml"));
	viewer.setMainQmlFile(QLatin1String("qml/kalenteri/main.qml"));
	viewer.showExpanded();

	//return app->exec();
	return app.exec();
}
