#ifndef CALENDAREVENT_H
#define CALENDAREVENT_H

#include <QObject>
#include <QDebug>
#include <QDateTime>

/**
 * @brief Represents a single event that has a starting and ending time along with some descriptive text.
 */
class CalendarEvent : public QObject
{
	Q_OBJECT

public:
	/**
	 * @brief Does nothing expect constructs QObject
	 * @param parent The parent QObject
	 */
	explicit CalendarEvent(QObject *parent = 0);

	/**
	 * @brief Google event Id
	 */
	QString eventId;
	/**
	 * @brief Time when the event begins.
	 */
	QDateTime timeBegins;
	/**
	 * @brief Time when the event ends.
	 */
	QDateTime timeEnds;
	/**
	 * @brief Long text describing this event.
	 */
	QString description;
	/**
	 * @brief Short text description of this event.
	 *
	 * This is considered to be a title or a subject for the event and thus is the main identifier for the user.
	 */
	QString summary;
};

#endif // CALENDAREVENT_H
