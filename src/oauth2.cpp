#include "oauth2.h"

const QString OAuth2::auth_endpoint = "https://accounts.google.com/o/oauth2/";

const QString OAuth2::client_id = "737737128343.apps.googleusercontent.com";
const QString OAuth2::redirect_uri = "urn:ietf:wg:oauth:2.0:oob";
const QString OAuth2::scope = "https://www.googleapis.com/auth/calendar";
const QString OAuth2::state = "StatusQuo";
const QString OAuth2::client_secret = "rvQ-HoT5lkX-IuusxDOwacxL";

const QString OAuth2::serializationFileLocation = "/.mozCalendar/";

OAuth2::OAuth2(QObject *parent) :
	QObject(parent)
{
	netAccessMgr = new QNetworkAccessManager();
	//parser = new QJson::Parser();

	connect(netAccessMgr, SIGNAL(finished(QNetworkReply*)), this, SLOT(handleNetworkReply(QNetworkReply*)));

	deserializeFromFile();
}

void OAuth2::handleNetworkReply(QNetworkReply* reply) {
	QByteArray replyArray = reply->readAll();
	qDebug() << "Network reply received";

	QJsonParseError parseError;
	QJsonDocument jsondoc = QJsonDocument::fromJson(replyArray, &parseError);
	QVariantMap jsonmap = jsondoc.toVariant().toMap();

	//if (!parseSuccess) {
	if (parseError.error != QJsonParseError::NoError) {
		qDebug() << "JSON parse fail!";
		qDebug() << replyArray << endl;
		return;
	}

	qDebug() << "JSON parse success! Size: " << jsonmap.size() << endl;
	if (jsonmap.size() == 0)
		return;

	// what is it?
	if (jsonmap["refresh_token"].isValid()) { // inital access token and refresh token
		access_token = jsonmap["access_token"].toString();
		refresh_token = jsonmap["refresh_token"].toString();
		expires = QDateTime::currentDateTimeUtc();
		expires.addSecs(jsonmap["expires_in"].toInt() - 20);
		serializeToFile();
	} else if (jsonmap["access_token"].isValid()) { // new access token after refresh request
		access_token = jsonmap["access_token"].toString();
		expires = QDateTime::currentDateTimeUtc();
		expires.addSecs(jsonmap["expires_in"].toInt() - 20);
	} else if (jsonmap["error"].isValid()) { // error reply
		qDebug() << "Got error reply from remote:" << endl << replyArray;
	} else { // something else that should be forwarded
		emit incomingData(jsonmap); // blindly send the json to all registered listeners
		// inefficient! Should be replaced with a more sophisticated forwarding scheme if more rescipients are added.
	}
}

void OAuth2::sendRequest(httpMethod method, QNetworkRequest request) {
	if (!isAuthorized())
		return;

	//request.setRawHeader(QString("Authorization").toAscii(), QString("Bearer " + access_token).toAscii());
	request.setRawHeader(QString("Authorization").toLatin1(), QString("Bearer " + access_token).toLatin1());

	if (method == GET) {
		netAccessMgr->get(request);
		qDebug() << "sent get: " << request.url().toString();
	}
}

void OAuth2::sendRequest(httpMethod method, QNetworkRequest request, QByteArray body) {
	if (!isAuthorized())
		return;

	//request.setRawHeader(QString("Authorization").toAscii(), QString("Bearer " + access_token).toAscii());
	request.setRawHeader(QString("Authorization").toLatin1(), QString("Bearer " + access_token).toLatin1());

	if (method == POST) {
		netAccessMgr->post(request, body);
		qDebug() << "sent post: " << request.url().toString() << endl << "body: " << body;
	}
}

bool OAuth2::isValidAccessToken() {
	if (access_token == "")
		return false;

	return true;
}

QString OAuth2::getAuthorizationURL() {
	QString url = auth_endpoint + "auth?";
	url += "response_type=code&client_id=" + client_id;
	url += "&redirect_uri=" + redirect_uri;
	url += "&scope=" + scope;

	return url;
}

void OAuth2::requestAccessToken() {
	QString postdata = "code=" + authorization_code;
	postdata += "&client_id=" + client_id;
	postdata += "&client_secret=" + client_secret;
	postdata += "&redirect_uri=" + redirect_uri;
	postdata += "&grant_type=authorization_code";

	qDebug() << "Requesting access token...";

	QNetworkRequest netReq = QNetworkRequest(QUrl(auth_endpoint + "token"));
	netReq.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");
	netAccessMgr->post(netReq, postdata.toLatin1());
}

void OAuth2::requestAccessTokenRefresh() {
	QString postdata = "refresh_token=" + refresh_token;
	postdata += "&client_id=" + client_id;
	postdata += "&client_secret=" + client_secret;
	postdata += "&grant_type=refresh_token";

	qDebug() << "Requesting access token refresh...";

	QNetworkRequest netReq = QNetworkRequest(QUrl(auth_endpoint + "token"));
	netReq.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");
	netAccessMgr->post(netReq, postdata.toLatin1());
}

void OAuth2::setAuthorizationCode(QString newAuthCode) {
	access_token = "";
	refresh_token = "";

	authorization_code = newAuthCode;
	qDebug() << "set new authorization code: " << authorization_code << endl;

	requestAccessToken();
}

bool OAuth2::isAuthorized() {
	if (authorization_code == "") {
		qDebug() << "authorization code is empty";
		emit authorizatioRequired();
		return false;
	} else if (refresh_token == "") {
		qDebug() << "access token is empty";
		requestAccessToken();
		return false;
	} else if (access_token == "") {
		requestAccessTokenRefresh();
		return false;
	}

	return true;
}

bool OAuth2::deserializeFromFile() {
	qDebug() << "deserializing oauth2";

	QFile file(QDir::homePath() + serializationFileLocation + "oauth2");
	if (!file.open(QFile::ReadOnly))
		return false;

	qDebug() << "oauth2 serialization file exists";
	QTextStream stream(&file);
	authorization_code = stream.readLine();
	refresh_token = stream.readLine();

	file.close();
	return true;
}

void OAuth2::serializeToFile() {
	qDebug() << "serializing oauth2";
	QDir dir(QDir::homePath() + serializationFileLocation);
	if (!dir.exists())
		dir.mkpath(QDir::homePath() + serializationFileLocation);

	QFile file(QDir::homePath() + serializationFileLocation + "oauth2");

	if (file.exists())
		file.remove();

	file.open(QFile::ReadWrite);
	QString data = authorization_code + "\n" + refresh_token;
	//file.write(data.toAscii());
	file.write(data.toLatin1());
	file.flush();
	file.close();

	qDebug() << "auth: " << authorization_code << " refresh: " << refresh_token;
}
