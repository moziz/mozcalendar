var searchData=
[
  ['redirect_5furi',['redirect_uri',['../classOAuth2.html#a06a007ca9fa1cdb6a3702615ae4addc0',1,'OAuth2']]],
  ['refresh',['refresh',['../classCalendar.html#a07cfd4ed85b28bfc9b97c1212b1869f5',1,'Calendar']]],
  ['refresh_5ftoken',['refresh_token',['../classOAuth2.html#acc546ac2ee33fadfb70758414e214085',1,'OAuth2']]],
  ['requestaccesstoken',['requestAccessToken',['../classOAuth2.html#ad8d1571af3f9a62c7702e573826b0e74',1,'OAuth2']]],
  ['requestaccesstokenrefresh',['requestAccessTokenRefresh',['../classOAuth2.html#ade66dfe7e6f00cbe4b9b9e72abdfdf1e',1,'OAuth2']]],
  ['requestauthorization',['requestAuthorization',['../classCalendar.html#ae1f3d279ae314d2074ce7c3611b5c713',1,'Calendar']]],
  ['roles',['roles',['../classCalendarData.html#a20fdb3ab5a089e91097d0daa394f6ab4',1,'CalendarData::roles()'],['../classDayViewDataModel.html#a9c69534beac7c2f421e36791add48731',1,'DayViewDataModel::roles()']]],
  ['rowcount',['rowCount',['../classCalendarData.html#ac7d3586710849027933fdeef0871d732',1,'CalendarData::rowCount()'],['../classDayViewDataModel.html#a6e55b3259dee4a4aa66f0453621f5e18',1,'DayViewDataModel::rowCount()']]]
];
