QT += webkit webkitwidgets network xml declarative

CONFIG += qt plugin #qws

#LIBS += -lqjson

# Add more folders to ship with the application, here
folder_01.source = qml/kalenteri
folder_01.target = qml
DEPLOYMENTFOLDERS = folder_01

# Additional import path used to resolve QML modules in Creator's code model
QML_IMPORT_PATH = qml/kalenteri

# If your application uses the Qt Mobility libraries, uncomment the following
# lines and add the respective components to the MOBILITY variable.
# CONFIG += mobility
# MOBILITY +=

# Speed up launching on MeeGo/Harmattan when using applauncherd daemon
# CONFIG += qdeclarative-boostable

# The .cpp file which was generated for your project. Feel free to hack it.

# Please do not modify the following two lines. Required for deployment.
#include(qmlapplicationviewer/qmlapplicationviewer.pri)
include(qtquick2applicationviewer/qtquick2applicationviewer.pri)
qtcAddDeployment()

#OTHER_FILES += \
#	qml/kalenteri/monthdaytile.qml

SOURCES += src/main.cpp \
src/calendardata.cpp \
src/calendar.cpp \
    src/oauth2.cpp \
    src/calendardaytile.cpp \
    src/calendarevent.cpp \
    src/calendardatacache.cpp \
    src/dayviewdatamodel.cpp

HEADERS += \
src/calendardata.h \
src/calendar.h \
    src/oauth2.h \
    src/calendardaytile.h \
    src/calendarevent.h \
    src/calendardatacache.h \
    src/dayviewdatamodel.h

OTHER_FILES +=
